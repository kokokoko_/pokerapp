//
//  PuzzleLayer.hpp
//  HelloWorld
//
//  Created by Ryoga Komine on 2016/06/15.
//
//

#ifndef PuzzleLayer_hpp
#define PuzzleLayer_hpp

#include "cocos2d.h"
#include "CardSprite.hpp"

class PuzzleLayer:public cocos2d::Layer{
private:
    std::vector<CardSprite*> _boardCard;//盤面に出ているカード[x,y]
    
    void setCard(PuzzleLayer*);
    void printCard();
    bool calcLine(cocos2d::Vec2*);
    int  calcHands(std::vector<CardSprite*>*);
    CardSprite* getCardSprite(cocos2d::Vec2*);

protected:
    PuzzleLayer();
    virtual ~PuzzleLayer();
    bool init() override;
public:
    bool onTouchBegan(cocos2d::Touch* touch,cocos2d::Event* event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
    
    CC_SYNTHESIZE_RETAIN(CardSprite *,_currentCard,CurrentCard);
    
    CREATE_FUNC(PuzzleLayer);
    
};

#endif /* PuzzleLayer_hpp */
