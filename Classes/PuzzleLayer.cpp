//
//  PuzzleLayer.cpp
//  HelloWorld
//
//  Created by Ryoga Komine on 2016/06/15.
//
//

#include "PuzzleLayer.hpp"
#include "CardSprite.hpp"
PuzzleLayer::PuzzleLayer()
:_currentCard(nullptr)
{
    
}
PuzzleLayer::~PuzzleLayer(){
    
}

bool PuzzleLayer::init(){
    if(!Layer::init()){
        return false;
    }
    
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(PuzzleLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(PuzzleLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(PuzzleLayer::onTouchEnded, this);
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);

    setCard(this);
    
    return true;
}

float cardSize = CardSprite::getSize();

void PuzzleLayer::setCard(PuzzleLayer *puzzleLayer){
    for(int i=0;i<5;i++){
        for(int j=0;j<5;j++){
            auto card = CardSprite::create();
            cocos2d::Vec2 index = cocos2d::Vec2((float)i,(float)j);
            float scale =cardSize/card->getContentSize().width;
            card->setScale(scale);
            card->setPosition(CardSprite::getCardPointByIndex(index));
            this->addChild(card);
            card->setCardIndex(index);
            _boardCard.push_back(card);
        }
    }
}

void PuzzleLayer::printCard(){
    for(CardSprite* card: _boardCard){
        card->setPosition(CardSprite::getCardPointByIndex(card->getCardIndex()));
    }

 }

bool PuzzleLayer::onTouchBegan(cocos2d::Touch* touch,cocos2d::Event* event){
    
    //画面をタッチした時の処理
    CCLOG("TOUCHSTART");
    auto location = touch->getLocation();
    
    auto cardIndex1 = CardSprite::getCardIndexByPoint(location);
    _currentCard = getCardSprite(&cardIndex1);
    
    return true;
    
}

void PuzzleLayer::onTouchMoved(cocos2d::Touch* touch,cocos2d::Event* event) {
    
    //タッチ中の処理
    auto location = touch->getLocation();
    CCLOG("TOUCHING");
    CCLOG("%f , %f",location.x,location.y);
}

void PuzzleLayer::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event){
    
    //タッチが終わった時の処理
    auto location = touch->getLocation();
    auto cardIndex2 = CardSprite::getCardIndexByPoint(location);
    CardSprite* swap_card = getCardSprite(&cardIndex2);
    CardSprite::swapCardIndex(_currentCard, swap_card);
    CCLOG("TOUCHED");
    calcLine(&cardIndex2);
}

CardSprite* PuzzleLayer::getCardSprite(cocos2d::Vec2* cardIndex){
    for(CardSprite* card: _boardCard){
        if(card->compareCardIndex(cardIndex)){
            return card;
        }
    }
    return nullptr;
}

bool PuzzleLayer::calcLine(cocos2d::Vec2* baseIndex){
    float base_x = baseIndex->x;
    float base_y = baseIndex->y;
    std::vector<CardSprite*> varticalLine(5);
    for(int i=0;i<5;i++){
        cocos2d::Vec2 idx = cocos2d::Vec2(base_x,i);
        CardSprite* c = getCardSprite(&idx);
        varticalLine.push_back(c);
    }
    CCLOG("%i",calcHands(&varticalLine));
    return true;
}

int  PuzzleLayer::calcHands(std::vector<CardSprite*>* hands){
    return 1;
}
