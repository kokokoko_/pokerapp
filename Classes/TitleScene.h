//
//  TitleScene.hpp
//  HelloWorld
//
//  Created by Ryoga Komine on 2016/06/14.
//
//

#ifndef TitleScene_hpp
#define TitleScene_hpp

#include "cocos2d.h"


class Title : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    //初期化のメソッド
    virtual bool init();
    
    //create()を使えるようにしている。
    CREATE_FUNC(Title);
    void pushStart(cocos2d::Ref *pSender); //スタートボタン押下時の処理宣言
};


#endif /* TitleScene_hpp */
