//
//  GameBoard.cpp
//  HelloWorld
//
//  Created by Ryoga Komine on 2016/06/15.
//
//

#include "GameBoardScene.hpp"
#include "CardSprite.hpp"
#include "PuzzleLayer.hpp"

USING_NS_CC;

Scene* GameBoardScene::createScene(){
    auto scene = Scene::create();
    auto layer = GameBoardScene::create();
    scene->addChild(layer);
    return scene;
}

bool GameBoardScene::init(){
    if(!Layer::init()){
        CCLOG("BOFAILED");
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize(); //画面のサイズを取得
    auto glview = Director::getInstance()->getOpenGLView();
    Point origin = Director::getInstance()->getVisibleOrigin();  //マルチレゾリューション対応がどうとか

    //パズルレイヤーの作成
    auto puzzleLayer = PuzzleLayer::create();
//    puzzleLayer->setScale(300/puzzleLayer->getContentSize().width);
    puzzleLayer->setContentSize(Size(300.0f,300.0f));
    puzzleLayer->setPosition(10,10);
    this->addChild(puzzleLayer);
    

    
    
    
    
    
    
    return true;
}


