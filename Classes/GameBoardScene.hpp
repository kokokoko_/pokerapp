//
//  GameBoard.hpp
//  HelloWorld
//
//  Created by Ryoga Komine on 2016/06/15.
//
//

#ifndef GameBoardScene_hpp
#define GameBoardScene_hpp

#include "cocos2d.h"
#include "PuzzleLayer.hpp"


class GameBoardScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    //初期化のメソッド
    virtual bool init();
    

    
    //create()を使えるようにしている。
    CREATE_FUNC(GameBoardScene);

};
#endif /* GameBoard_hpp */
