//
//  CardSprite.cpp
//  HelloWorld
//
//  Created by Ryoga Komine on 2016/06/15.
//
//

#include "CardSprite.hpp"
#include "GameBoardScene.hpp"
#include <algorithm>
#include <random>
USING_NS_CC;

CardSprite::CardSprite(){
}
CardSprite::~CardSprite(){
    
}

//カードIndex
cocos2d::Vec2 cardIndex;


bool CardSprite::init() {
    cardSet = getNextCardSet();
    if(!Sprite::initWithFile(cardSet.filename)){
        return false;
    }
    return true;
}


std::vector<CardSprite::_CardSet> CardSprite::cardStack;

CardSprite::_CardSet CardSprite::getNextCardSet(){
    CardSprite::_CardSet t_cardset;
    std::vector<CardSprite::_CardSet> *cardStack = &CardSprite::cardStack;
    if(cardStack->empty()){
        for(std::string c:{"s","c","h","d"}){
            for(int s=1;s<=13;s++){
                t_cardset.filename = "trump/"+c+StringUtils::toString(s)+".png";
                t_cardset.mark = c;
                t_cardset.number = s;
                cardStack->push_back(t_cardset);
            }
        }
        std::random_device rdev{}; //乱数生成器を生成
        std::mt19937 mt(rdev()); //メルセンヌ・ツイスタ生成器を使用
        std::shuffle(cardStack->begin(), cardStack->end(), mt);//Vectorをシャッフル！
    }
    auto next = cardStack->back();
    cardStack->pop_back();
    return next;
}

void CardSprite::swapCardIndex(CardSprite* c1,CardSprite* c2){
    if(c1==0||c2==0){
        return;
    }else if(c1->getNumberOfRunningActions() != 0 || c2->getNumberOfRunningActions() != 0){
        return;
    }
    c1->setPosition(CardSprite::getCardPointByIndex(c1->getCardIndex()));
    c2->setPosition(CardSprite::getCardPointByIndex(c2->getCardIndex()));
    
    CCActionInterval* action1 = CCMoveBy::create(0.2, c2->getPosition() - c1->getPosition());
    CCActionInterval* action2 = CCMoveBy::create(0.2, c1->getPosition() - c2->getPosition());
    
    
    Vec2 tmp = c1->getCardIndex();
    c1->setCardIndex(c2->getCardIndex());
    c2->setCardIndex(tmp);

    c1->runAction(action1);
    c2->runAction(action2);
}

Vec2 CardSprite::getCardIndexByPoint(const cocos2d::Vec2 &cardPosition){
    int x = floor((cardPosition.x-10)/CardSprite::getSize());
    int y = floor((cardPosition.y-10)/CardSprite::getSize());
    return std::move(Vec2(x,y));
}
Vec2 CardSprite::getCardPointByIndex(const cocos2d::Vec2 &cardPosition){
    float x = cardPosition.x*CARD_SIZE+CARD_SIZE/2;
    float y = cardPosition.y*CARD_SIZE+CARD_SIZE/2;
    return std::move(Vec2(x,y));
}