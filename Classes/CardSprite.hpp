//
//  CardSprite.hpp
//  HelloWorld
//
//  Created by Ryoga Komine on 2016/06/15.
//
//

#ifndef CardSprite_hpp
#define CardSprite_hpp

#include "cocos2d.h"

const float CARD_SIZE = 60;  //カードの１辺の大きさ

class CardSprite:public cocos2d::Sprite{
    struct _CardSet{
        std::string filename;
        std::string mark;
        int number;
    };

private:

    //カードIndex
//    cocos2d::Vec2* cardIndex;
    std::string cardName;
    CardSprite::_CardSet cardSet;
    unsigned int random (unsigned int max);
protected:
    CardSprite();
    virtual ~CardSprite();
    bool init() override;

public:

    
    CardSprite::_CardSet getNextCardSet();
//    void setCardIndex(cocos2d::Vec2*);
    
/*    cocos2d::Vec2* getCardIndex(){
        return cardIndex;
    }
*/
    bool compareCardIndex(cocos2d::Vec2 *index){
        return cardIndex.x == index->x && cardIndex.y==index->y;
    }
    
    std::string* getCardName(){
        return &cardSet.filename;
    }
    
    
    
    //カードの大きさ
    static float getSize(){
        return CARD_SIZE;
    }
    
    //カードスタック
    static std::vector<CardSprite::_CardSet> cardStack;

    //座標からクッキーのindexを取得する
    static cocos2d::Vec2 getCardIndexByPoint(const cocos2d::Vec2& cardPosition);

    //クッキーのindexから座標を取得する
    static cocos2d::Vec2 getCardPointByIndex(const cocos2d::Vec2& cardPosition);
    
    static void swapCardIndex(CardSprite* c1,CardSprite* c2);
    
    CREATE_FUNC(CardSprite);
    CC_SYNTHESIZE(cocos2d::Vec2, cardIndex, CardIndex);
};
#endif /* CardSprite_hpp */
